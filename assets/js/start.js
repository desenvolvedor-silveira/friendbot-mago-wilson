var Start = new Phaser.Class({
    Extends: Phaser.Scene,
    
    initialize: function (config)
    {
        Phaser.Scene.call(this, {
            key: "start",
            active: true,
        });
    },

    preload: function ()
    {
        this.load.image('bg_start', 'assets/img/bg_start.jpg');
    },

    create: function ()
    {
        this.add.image(0, -100, 'bg_start').setOrigin(0, 0).setScale(.8);

        const button = this.add.text(430, 400, "Iniciar jogo", {
            fontSize: "28pt",
            color: "#fff",
            backgroundColor: "#000"
        })
            .setInteractive()
            .on("pointerdown", () => button.setScale(1.1))
            .on("pointerup", () => {
                button.setScale(1.0);
                this.scene.stop("start").start("intro");
            });

        this.add.text(430, 150, "Mago Wilson", {
            fontSize: "38pt",
            color: "#fff",
            backgroundColor: "#000"
        })
        this.add.text(430, 200, "E as reliquias misticas", {
            fontSize: "18pt",
            color: "#fff",
            backgroundColor: "#000"
        })
    },

    update: function ()
    {
    }
});
