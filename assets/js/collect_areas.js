const CollectAreas = {
    debug: false,
    areas: [
        {
            poly: [[597, 287], [607, 250], [630, 261], [617, 302], [597, 287]],
            message: "Vejo que voce encontrou o cogumelo da floresta. Mas quando foi que eu deixei ele atras dessa pintura?",
            collectable: true,
            item: "mushroom"
        },

        {
            poly: [[100, 180], [140, 100], [180,120], [150, 150]],
            message: "Eu ja ia limpar essa teia de aranha, mas parece que voce conseguiu encontrar a aranha saltadora. Excelente!",
            collectable: true,
            item: "spider",
        },

        {
            poly: [[487, 262], [504, 262], [504, 270], [487, 270]],
            message: "Legal! Voce encontrou o meu dente de dragao... Pra que isso servia mesmo?",
            collectable: true,
            item: "tooth",
        },

        {
            poly: [[487, 272], [504, 272], [504, 280], [487, 280]],
            message: "Hmmm, nessa gaveta so tem papel e uma foto da minha tia Astrobalda. Essa verruga no nariz dela era bem maior pessoalmente!",
            collectable: false,
            item: "",
        },

        {
            poly: [[390, 350], [410,340], [440, 320], [420, 320]],
            message: "Magnifico! Vejo que voce encontrou um punhado de poeira estelar! Voce e muito inteligente mesmo!",
            collectable: true,
            item: "powder",
        },

        {
            poly: [[90, 300], [80, 300], [87, 275], [110, 250]],
            message: "Ah, mas que descuido meu! Quase que uso a pena de fenix para escrever! Ainda bem que voce a encontrou antes!",
            collectable: true,
            item: "feather",
        },
        
        {
            poly: [[290, 345], [290,325], [295, 308], [322, 313], [335, 326], [335, 350], [310, 355]],
            message: "Nesse bau velho, nao tem nada mesmo! So um monte de coisa velha... Nao, minha avo nao esta ai!",
            collectable: false,
            item: "",
        },
    ]
};
