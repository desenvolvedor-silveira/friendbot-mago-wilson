var Intro = new Phaser.Class({
    Extends: Phaser.Scene,
    
    initialize: function (config)
    {
        Phaser.Scene.call(this, {
            key: "intro",
            active: false,
        });
    },

    preload: function ()
    {
        const button = this.add.text(260, 350, "Continuar >>", {fontSize: "18pt", color: "#000", backgroundColor: "#ffcc00"})
            .setInteractive()
            .on("pointerdown", () => button.setScale(1.1))
            .on("pointerup", () => {
                button.setScale(1.0);
                this.scene.start("collect");
            });

        const introText = [
            "Oi, eu sou o Mago Wilson,",
            "Eu gosto muito de estudar e leio bastante sobre feiticos.",
            "",
            "So que em uma das minhas experiencias magicas,",
            "algo acabou dando errado, e agora eu preciso da ajuda de",
            "uma crianca corajosa e inteligente.",
            "",
            "Acho que acabei de encontrar uma, voce!",
            "",
            "Me ajude a encontrar os ingredientes para criar minha pocao",
            "e fazer com que os objetos no meu laboratorio voltem ao normal.",
        ];
 


        this.add.text(50, 60, introText, {fontSize: "14pt", color: "#fff", backgroundColor: "transparent", wordWrap: true, wordWrapWidth: 300});
    },

    create: function ()
    {
    },

    update: function ()
    {
    }
});
