var Collect = new Phaser.Class({
    Extends: Phaser.Scene,
    
    initialize: function (config)
    {
        Phaser.Scene.call(this, {
            key: "collect",
            active: false,
        });
    },

    preload: function ()
    {
        this.load.image('labo', 'assets/img/labo.jpg');
        
        this.load.image('banner', 'assets/img/collect_banner.png');
        this.load.image('feather', 'assets/img/feather.png');
        this.load.image('mushroom', 'assets/img/mushroom.png');
        this.load.image('powder', 'assets/img/powder.png');
        this.load.image('spider', 'assets/img/spider.png');
        this.load.image('tooth', 'assets/img/tooth.png');

        this.load.audio('sugar_plum_fairy', 'assets/audio/sugar_plum.mp3', {instances: 1});
        this.load.audio('searching', 'assets/audio/searching.mp3');
    },

    create: function ()
    {
        this.createScene();
        this.createIngredientsBar();
        this.createCollectAreas();
    },

    update: function ()
    {
        this.updateIngredientsBar()
        this.updateCollectAreas();
    },

    createScene: function ()
    {
        this.theme = this.sound.add('sugar_plum_fairy');
        this.theme.loop = true;
        this.theme.play();
        
        let labo = this.add.image(400, 240, 'labo');
        labo.setPipeline('Light2D');
        this.add.text(15, 15, "Colete todos os ingredientes", {fontSize: "12pt"});

        this.lights.enable().setAmbientColor(0x303030);
        let lantern = this.lights.addLight(0, 0, 300, 0xffffff, 2);

        this.input.on('pointermove', function (pointer) {
            lantern.x = pointer.x;
            lantern.y = pointer.y;
        });

        this.lantern = lantern;
    },

    createIngredientsBar: function ()
    {
        this.ingredients = {
            feather: {
                index: 1,
                img: null,
                collected: false,
            },
            mushroom: {
                index: 2,
                img: null,
                collected: false,
            },
            powder: {
                index: 3,
                img: null,
                collected: false,
            },
            spider: {
                index: 4,
                img: null,
                collected: false,
            },
            tooth: {
                index: 5,
                img: null,
                collected: false,
            }
        };

        this.add.image(520, 10, 'banner').setOrigin(0, 0).setScale(.5).setAlpha(.7);

        for (let i in this.ingredients) {
            this.ingredients[i].img = this.add.image(500 + (this.ingredients[i].index * 50), 25, i);
            this.ingredients[i].img.scale = .2;
            this.ingredients[i].img.alpha = .6;
        }
    },

    createCollectAreas: function ()
    {
        this.searchingSound = this.sound.add('searching');

        this.collectAreas = [];

        for (let i in CollectAreas.areas) {
            let graph = this.add.graphics();
            let area = CollectAreas.areas[i];
            let poly = new Phaser.Geom.Polygon();
            let points = [];

            if (!this.isValidCollectArea(area)) {
                continue;
            }

            graph.fillStyle(0x000000, .0);
            graph.lineStyle(0, 0xffffff, 1);
            if (CollectAreas.debug) {
                graph.fillStyle(0x00ff00, .1);
                graph.lineStyle(1, 0xffffff, 1);
            }

            for (let q in area.poly) {
                let p = area.poly[q];
                points.push(new Phaser.Geom.Point(p[0], p[1]));
            }

            poly.setTo(points);

            graph.setInteractive(poly, (p, x, y) => p.contains(x, y), {cursor: 'pointer'})
                .on("pointerover", () => {
                    graph.clear();
                    graph.fillStyle(0xffffff, .0);
                    graph.lineStyle(1, 0xffcc00, .2);
                })
                .on("pointerout", () => {
                    graph.clear();
                    graph.fillStyle(0xffffff, .0);
                    graph.lineStyle(0, 0xffffff, .0);
                })
                .on("pointerdown", () => {
                    let isCollectable = area.collectable && area.item in this.ingredients;

                    if (isCollectable && this.ingredients[area.item].collected) {
                        return true;
                    }
                    
                    if (isCollectable) {
                        this.ingredients[area.item].collected = true;
                    }

                    showMessage(area.message);
                });


            this.collectAreas.push({poly: poly, graph: graph});
        }
    },

    isValidCollectArea: function (area)
    {
        return area.poly.length > 0
            && (!area.collectable || area.item.length > 0)
    },

    updateIngredientsBar: function ()
    {
        allCollected = true;
    
        for (let i in this.ingredients) {
            if (this.ingredients[i].collected) {
                this.ingredients[i].img.alpha = 1;
                this.ingredients[i].img.scale = .3;
                continue;
            }
    
            allCollected = false;
        }
    
    
        if (allCollected) {
            this.theme.stop();
            this.scene.stop("collect");
            this.scene.start("final");
        }
    },

    updateCollectAreas: function ()
    {
        for (i in this.collectAreas) {
            let area = this.collectAreas[i];
            area.graph.strokePoints(area.poly.points, true);
        }
    }
});

function showMessage(msg)
{
    // TODO: implementar um mensagem na tela do game, usando as ferramentas do Phaser.
    alert(msg);
}
